import { useDispatch } from 'react-redux'
import { addProduct } from '../../store/reducers/basket'
import './card.css'

function Card({ id, img, name, description, price, weight }) {
    const dispatch = useDispatch()
    let item = {
        id: id,
        img: img,
        name: name,
        price: price,
        weight: weight,

    }
    const handleAddproduct = () => dispatch(addProduct(item));

    return (
        <div className="card">
            <img className="card__preview" src={img}
                alt="" />
            <h2 className="card__title">
                {name}
            </h2>
            <p className="card__description">
                {description}
            </p>
            <div className="card__price">
                {price} ₽/
                <sub>{weight}</sub>
                <div className="card__add-button"  onClick={handleAddproduct}>
<svg width="30" height="30" viewBox="0 0 30 30" fill="#323232" xmlns="http://www.w3.org/2000/svg">
<circle cx="15" cy="15" r="15" />
<path d="M15 9.28564V20.3571" stroke="white" stroke-width="2" stroke-linecap="round"/>
<path d="M20.3569 14.8214L9.28551 14.8213" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
</div>


            </div>
        </div>
    );
}
export default Card;
