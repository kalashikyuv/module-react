import { createSlice } from "@reduxjs/toolkit";

const basketSlise = createSlice({
    name: 'basket',
    initialState: {
        basket: [],
        priceProducts: 0,
    },
    reducers: {
        addProduct(state, action) {
            state.basket.push(action.payload);
            state.basket.sort((a, b) => a.id - b.id);

            state.priceProducts = state.basket.reduce((sum, current) => {
                return sum + +current.price
            }, 0)

        },

        removeProductBasket(state, action) {
            const index = state.basket.findIndex(elem => elem.id === action.payload)
            state.basket.splice(index, 1);

            state.priceProducts = state.basket.reduce((sum, current) => {
                return sum + +current.price
            }, 0)

        }
    }
})
export const { addProduct, removeProductBasket } = basketSlise.actions
export default basketSlise.reducer
