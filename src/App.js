import './App.css';
import Basket from './Pages/Basket';
import Products from './Pages/Products';
import {Routes, Route, Link} from 'react-router-dom';
function App (){
  return (
    <div className='App'>
      <Routes>
      <Route path='/' element={<Products/>}/>
      <Route path= '/basket' element={<Basket/>}/>
      </Routes>
    </div>

  );
 }   

 

export default App;
