import "./Basket.css"
import { Link } from "react-router-dom";
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { removeProductBasket } from '../store/reducers/basket'
import { v4 as uuidv4 } from 'uuid'

function Basket() {

  const dispatch = useDispatch()
  const priceProducts = useSelector(state => state.basket.priceProducts);

  const handleRemProduct = (id) => {
    dispatch(removeProductBasket(id))
  }

  const basket = useSelector(state => state.basket.basket)

  return (


    <div className={"cart"}>
      <header className='cart__header'>
        <div className='cart__backButton'><Link to='/'> <img src='../../images/back.png'></img></Link></div>
        <h1 className={"cart__title"}>КОРЗИНА С ВЫБРАННЫМИ ТОВАРАМИ</h1>
      </header>
      {basket.map(item => {
        const { id, img, name, price } = item
        return (
          <div className='cart__card' key={uuidv4()}>

            <div className='cart__item-wrap'>
              <div className='cart__item-image'><img src={img} alt='изображение товара' /></div>
              <div className='cart__item-title'>{name}</div>
              <div className='cart__price-wrap'>
                <div className='cart__item-price'>{price} ₽</div>
                <img className='cart__delete-button' src='../../images/delete.png' onClick={() => handleRemProduct(id)}></img>

              </div>
            </div>
          </div>
        )

      })}

      <main>

      </main>
      <div className='cart__footerLine'><hr></hr></div>
      <footer className='cart__footer'>
        <h2 className='cart__price'>Заказ на сумму: <span>{priceProducts} ₽</span></h2>
        <button className='cart__button'>Оформить заказ</button>

      </footer>

    </div>


  )
}

export default Basket;
