import { produks } from "../produks"
import Card from "../components/elements/card"
import Header from "./../components/header/Header.jsx"
import { useDispatch } from 'react-redux'
import { removeProductBasket } from '../store/reducers/basket'
import { useSelector } from 'react-redux'

import { Link } from "react-router-dom"
import { v4 as uuidv4 } from 'uuid'

import "./Products.css"

function Products() {
  const dispatch = useDispatch()
  const priceProducts = useSelector(state => state.basket.priceProducts);
  return (
    <main className="main">
      <div className="container">
        <header className='header'>
          <h1>наша продукция</h1>
          <div className="basketArea">
            <div className="menu__cart"><Link to='/basket' className= 'menu__Link'><img src="../../images/5.png"></img></Link>
          </div></div>
          <h2 className='cart__price'>Заказ на сумму: <span>{priceProducts} ₽</span></h2>
        </header>

        <div className="menu">
          {produks.map(key => {
            return (
              <Card
                key={uuidv4()}
                id={key.id}
                img={key.img}
                name={key.name}
                description={key.description}
                price={key.price}
                weight={key.weight}

              />
            )

          })}
        </div>
      </div>
    </main>
  );
}
export default Products

